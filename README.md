# digit_recognition

An implmeentation to a very simple neural network in C++ for learning purposes.

Recognizes images of numbers by training on the MNIST dataset.

You need to have git LFS installed if you wanna clone this project.

Uses [Eigen](https://eigen.tuxfamily.org/index.php?title=Main_Page) for vector/matrix multiplications and [wichtounet/mnist](https://github.com/wichtounet/mnist) to read the MNIST dataset.