#include "network.h"
#include "utility_functions.h"

#include <algorithm>
#include <random>
#include <iostream>

using namespace std;
using namespace Eigen;

namespace AdamNN
{

Network::Network(const vector<size_t>& sizes) :
	m_num_layers(sizes.size()),
	m_sizes(sizes)
{
	for(size_t i = 1; i < m_sizes.size(); i++)
	{
		m_biases.push_back(MatrixXd::Random(1, long(m_sizes[i])));
		m_weights.push_back(MatrixXd::Random(long(m_sizes[i - 1]),
											 long(m_sizes[i])));
	}
}

RowVectorXd sigmoid(Eigen::RowVectorXd z)
{
	for(int i = 0; i < z.cols(); i++)
	{
		z[i] = 1.0/(1.0+exp(z[i]));
	}

	return z;
}

RowVectorXd sigmoid_prime(RowVectorXd z)
{
	RowVectorXd x = z;

	for(int i = 0; i < z.cols(); i++)
	{
		z[i] = 1 - z[i];
	}

	return sigmoid(x).array() * sigmoid(z).array();
}

RowVectorXd Network::feed_forward(RowVectorXd a) const
{
	for(size_t i = 0; i < m_biases.size(); i++)
	{
		Eigen::RowVectorXd a_w = a * m_weights[i];
		Eigen::RowVectorXd a_w_b = a_w + m_biases[i];
		a = sigmoid( a_w_b );
	}

	return a;
}

uint8_t Network::feed_forward(std::vector<uint8_t> input) const
{
	if(input.size() != 784) throw std::runtime_error("input does not have the correct size");

	Eigen::RowVectorXd image_data(784);

	for(size_t i = 0; int(i) < image_data.cols(); i++)
	{
		image_data[int(i)] = input[i];
	}

	Eigen::RowVectorXd output_vector = feed_forward(image_data);

	return uint8_t(output_vector.maxCoeff());
}

size_t Network::evaluate(const Data& test_data) const
{
	int sum = 0;

	for(const auto& p : test_data)
	{
		sum += (feed_forward(p.first) == p.second);
	}

	return size_t(sum);
}

Eigen::RowVectorXd input_layer_to_matrix(std::vector<uint8_t> input_layer)
{
	Eigen::RowVectorXd output = Eigen::RowVectorXd::Zero(long(input_layer.size()));

	for(size_t i = 0; int(i) < output.cols(); i++)
	{
		output[int(i)] = input_layer[i];
	}

	return output;
}

Eigen::RowVectorXd cost_derivative(Eigen::RowVectorXd output_activations, Eigen::RowVectorXd expected)
{
	return output_activations - expected;
}

Eigen::RowVectorXd create_vector_from_label(uint8_t label)
{
	Eigen::RowVectorXd output = RowVectorXd::Zero(10);
	output[int(label)] = 1;
	return output;
}

std::pair<std::vector<MatrixXd>, std::vector<MatrixXd> >
	Network::backpropagate(const std::pair<std::vector<uint8_t>, uint8_t>& input_label)
{
	std::vector<Eigen::MatrixXd> new_biases = m_biases;
	std::vector<Eigen::MatrixXd> new_weights = m_weights;

	init_matrices_to_zero(new_biases, new_weights);

	Eigen::RowVectorXd activation = input_layer_to_matrix(input_label.first);
	std::vector<Eigen::RowVectorXd> activations;
	std::vector<Eigen::RowVectorXd> zs;

	activations.push_back(
						activation
					);

	for(size_t i = 0; i < m_biases.size(); i++)
	{
		Eigen::RowVectorXd a_w = activation * m_weights[i];
		Eigen::RowVectorXd z = a_w + m_biases[i];
		zs.push_back(z);
		activation = sigmoid(z);
		activations.push_back(activation);
	}

	Eigen::MatrixXd deltas = cost_derivative(activations.back(),
											 create_vector_from_label(input_label.second)).array()
							 * sigmoid_prime(zs.back()).array();

	new_biases.back() = deltas;
	new_weights.back() = smart_index(activations, -2).transpose() * deltas;

	for(size_t i = 2; i < m_num_layers; i++)
	{
		Eigen::RowVectorXd z = smart_index(zs, -int(i));
		Eigen::RowVectorXd sp = sigmoid_prime(z);
		deltas = (deltas * smart_index(m_weights, -int(i)+1).transpose()).array() * sp.array();
		smart_index(new_biases, -int(i)) = deltas;
		smart_index(new_weights, -int(i)) = smart_index(activations, -int(i)-1).transpose() * deltas;
	}

	return make_pair(new_biases, new_weights);
}

using namespace std::experimental;

void Network::update_mini_batch(const Data& batch, float learning_rate)
{
	std::vector<Eigen::MatrixXd> new_biases = m_biases;
	std::vector<Eigen::MatrixXd> new_weights = m_weights;

	init_matrices_to_zero(new_biases, new_weights);

	vector<vector<Eigen::MatrixXd>> all_delta_biases(batch.size());
	vector<vector<Eigen::MatrixXd>> all_delta_weights(batch.size());

#pragma omp parallel for
	for(size_t i = 0; i < batch.size(); i++)
	{
		std::vector<Eigen::MatrixXd> delta_biases(m_biases.size());
		std::vector<Eigen::MatrixXd> delta_weights(m_weights.size());

		std::tie(delta_biases, delta_weights) = backpropagate(batch[i]);

		all_delta_biases[i] = delta_biases;
		all_delta_weights[i] = delta_weights;
	}

	for(size_t i = 0; i < batch.size(); i++)
	{
		new_biases = zip_n_sum(new_biases, all_delta_biases[i]);
		new_weights = zip_n_sum(new_weights, all_delta_weights[i]);
	}

	auto do_for_each = [learning_rate, &batch] (Eigen::MatrixXd mat, Eigen::MatrixXd new_mat) -> Eigen::MatrixXd
	{
		return mat - (learning_rate/batch.size()) * new_mat;
	};

	m_weights = zip_n_for_each(m_weights, new_weights, do_for_each);
	m_biases = zip_n_for_each(m_biases, new_biases, do_for_each);
}

std::vector<Data> create_mini_batches(const Data& training_data, size_t batch_size)
{
	std::vector<Data> batches((training_data.size() + batch_size) / batch_size);

	for(size_t i = 0; i < training_data.size(); i += batch_size)
	{
		auto last = std::min(training_data.size(), i + batch_size);
		auto index = i / batch_size;
		auto& vec = batches[index];
		vec.reserve(last - i);
		move(training_data.begin() + long(i), training_data.begin() + long(last), back_inserter(vec));
	}

	return batches;
}

void Network::gradient_descent(Data training_data, size_t epochs,
							   size_t batch_size,
							   float learning_rate,
							   const optional<Data>& test_data)
{
	auto random_engine = std::default_random_engine {};

	for(size_t i = 0; i < epochs; i++)
	{
		std::shuffle(std::begin(training_data), std::end(training_data), random_engine);
		std::vector<Data> mini_batches = create_mini_batches(training_data, batch_size);

		for(const Data& batch : mini_batches)
		{
			update_mini_batch(batch, learning_rate);
		}

		if(test_data)
		{
			cout << "Epoch " << i << "/" << epochs <<
					" : " << evaluate(*test_data) << "/" << test_data->size() << " :)\n";
		}
		else
		{
			cout << "Epoch " << i << "/" << epochs << " complete :)\n";
		}
	}
}

} // namespace AdamNN
