#ifndef NETWORK_H
#define NETWORK_H

#include <vector>
#include <cstddef>
#include <Eigen/Dense>
#include <experimental/optional>

namespace AdamNN
{

typedef std::vector<std::pair<std::vector<uint8_t>, uint8_t>> Data;

class Network
{
	size_t m_num_layers;
	std::vector<size_t> m_sizes;
	std::vector<Eigen::MatrixXd> m_biases;
	std::vector<Eigen::MatrixXd> m_weights;

public:
	Network(const std::vector<size_t>& m_sizes);

	uint8_t feed_forward(std::vector<uint8_t> input) const;
	Eigen::RowVectorXd feed_forward(Eigen::RowVectorXd a) const;

	size_t evaluate(const Data& test_data) const;

	std::pair<std::vector<Eigen::MatrixXd>, std::vector<Eigen::MatrixXd>>
		backpropagate(const std::pair<std::vector<uint8_t>, uint8_t>& input_label);

	void update_mini_batch(const Data& batch, float learning_rate);
	void gradient_descent(Data training_data, size_t epochs, size_t batch_size, float learning_rate,
						  const std::experimental::optional<Data>& test_data = std::experimental::nullopt);
};

} // AdamNN

#endif // NETWORK_H
