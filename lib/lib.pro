TEMPLATE = lib
CONFIG += staticlib c++14
QMAKE_CXXFLAGS += -fPIC -fopenmp
CONFIG -= qt
TARGET = adam_nn

SOURCES += \
    network.cpp

HEADERS += \
    network.h \
    utility_functions.h

LIBS += -fopenmp
