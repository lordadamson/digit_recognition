#ifndef UTILITY_FUNCTIONS_H
#define UTILITY_FUNCTIONS_H

#include <vector>
#include <functional>

#include <Eigen/Dense>

/// Allows for negative indices like in python where v[-1] is the last element in v
template <typename T>
T& smart_index(std::vector<T>& v, int i)
{
	if(i >= 0)
	{
		return v[i];
	}

	return v[v.size() + i];
}

template<typename T1, typename T2>
std::vector<std::pair<T1, T2>> zip(const std::vector<T1>& x, const std::vector<T2>& y)
{
	if(x.size() != y.size())
	{
		throw std::runtime_error("zip arguments must have equal size.");
	}

	std::vector<std::pair<T1, T2>> output(x.size());

	for(size_t i = 0; i < x.size(); i++)
	{
		output[i] = std::make_pair(x[i], y[i]);
	}

	return output;
}

template<typename T1, typename T2>
std::vector<T1> zip_n_sum(const std::vector<T1>& x, const std::vector<T2>& y)
{
	std::vector<std::pair<T1, T2>> zipped = zip(x, y);
	std::vector<T1> summed(zipped.size());

	for(size_t i = 0; i < summed.size(); i++)
	{
		summed[i] = zipped[i].first + zipped[i].second;
	}

	return summed;
}

template<typename T1, typename T2, typename Functor>
std::vector<T1> zip_n_for_each(const std::vector<T1>& x, const std::vector<T2>& y, Functor do_for_each)
{
	std::vector<std::pair<T1, T2>> zipped = zip(x, y);
	std::vector<T1> output(zipped.size());

	for(size_t i = 0; i < output.size(); i++)
	{
		output[i] = do_for_each(zipped[i].first, zipped[i].second);
	}

	return output;
}

inline void init_matrices_to_zero(std::vector<Eigen::MatrixXd>& mat)
{
	for(auto& m : mat)
	{
		m = Eigen::MatrixXd::Zero(m.rows(), m.cols());
	}
}

template<typename ...Args>
void init_matrices_to_zero(std::vector<Eigen::MatrixXd>& first_mat, Args&... args)
{
	init_matrices_to_zero(first_mat);
	init_matrices_to_zero(args...);
}


#endif // UTILITY_FUNCTIONS_H
