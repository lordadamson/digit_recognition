#define STB_IMAGE_WRITE_IMPLEMENTATION

#include <iostream>
#include <string>

#include "mnist/mnist_reader.hpp"
#include <Eigen/Dense>
#include <stb_image_write.h>

#include <network.h>
#include <utility_functions.h>

using namespace std;
using namespace AdamNN;

const string MNIST_DATA_LOCATION = std::string(CODE_WORKING_DIR) + "mnist_data/";

int main()
{
	 //Load MNIST data
	mnist::MNIST_dataset<std::vector, std::vector<uint8_t>, uint8_t> dataset =
		mnist::read_dataset<std::vector, std::vector, uint8_t, uint8_t>(MNIST_DATA_LOCATION);

	Data training_data = zip(dataset.training_images, dataset.training_labels);
	Data test_data = zip(dataset.test_images, dataset.test_labels);

	Eigen::RowVectorXd image_data(784);

	for(size_t i = 0; int(i) < image_data.cols(); i++)
	{
		image_data[int(i)] = dataset.training_images[0][i];
	}

	vector<size_t> sizes = {784, 20, 10};
	Network nn(sizes);

	nn.gradient_descent(training_data, 30, 10, 3.0, test_data);

	cout << nn.feed_forward(image_data) << '\n';

	return 0;
}
