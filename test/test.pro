TEMPLATE = app
CONFIG += console c++1z
CONFIG -= app_bundle
CONFIG -= qt
QMAKE_CXXFLAGS += -fopenmp

DEFINES += CODE_WORKING_DIR=\\\"$$PWD/\\\"

SOURCES += main.cpp

HEADERS += \
    stb_image_write.h

INCLUDEPATH += $$PWD/../lib

LIBS += -L$$OUT_PWD/../lib -ladam_nn -fopenmp



